CONTIKI_PROJECT = level-mote unicast-receiver aval-mote

all: $(CONTIKI_PROJECT)

SMALL = 1

APPS += er-coap
APPS += rest-engine

UIP_CONF_IPV6=1
#WITH_UIP6 = 1

CFLAGS += -DUIP_CONF_IPV6_RPL
CFLAGS += -DPROJECT_CONF_H=\"project-conf.h\"

CONTIKI = /home/user/contiki

CONTIKI_WITH_IPV6 = 1

include $(CONTIKI)/Makefile.include
