#include "contiki.h"
#include "contiki-net.h"
#include "lib/random.h"
#include "sys/etimer.h"
#include "net/ip/uip.h"
#include "net/ip/uip-debug.h"
#include "net/rpl/rpl.h"

#include "simple-udp.h"

#include "rest-engine.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#define UDP_PORT 1234

#define	PERIOD			(CLOCK_SECOND * 10)
#define	SNOW_OFFSET		3		// from 3 to (SNOW_DIM + 2): snow level motes
#define	AVAL_OFFSET		(SNOW_DIM + 3)	// from (SNOW_DIM + SNOW_OFFSET) to (SNOW_DIM + SNOW_OFFSET + 1 + AVAL_DIM): avalanche motes
#define SNOW_DIM		4
#define	AVAL_DIM		4		// 4 neve, 4 valanghe


int snow_level[SNOW_DIM];
int aval_prob[AVAL_DIM];
int snow_gun_array[SNOW_DIM];


static struct simple_udp_connection unicast_connection;

void get_prob(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
void get_level(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
void get_gun_status(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
void post_handler (void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);

/*
static void get_prob_per();
static void get_level_per();
static void get_gun_status_per();
*/

static void get_prob_trigger();
static void get_level_trigger();
static void get_gun_status_trigger();


EVENT_RESOURCE(snow_level_mote, "title=\"Snow Level Resource\";rt=\"Text\";obs", get_level, post_handler, NULL, NULL, get_level_trigger);
EVENT_RESOURCE(avalanche_mote, "title=\"Avalanche Probability Resource\";rt=\"Text\";obs", get_prob, NULL, NULL, NULL, get_prob_trigger);
EVENT_RESOURCE(snow_gun, "title=\"Snow Gun Resource\";rt=\"Text\";obs", get_gun_status, post_handler, NULL, NULL, get_gun_status_trigger);
/*
PERIODIC_RESOURCE(snow_level_mote, "title=\"Snow Level Periodic Resource\";rt=\"Text\";obs", get_level, post_handler, NULL, NULL, PERIOD, get_level_per);
PERIODIC_RESOURCE(avalanche_mote, "title=\"Avalanche Periodic Resource\";rt=\"Text\";obs", get_prob, NULL, NULL, NULL, PERIOD, get_prob_per);
PERIODIC_RESOURCE(snow_gun, "title=\"Snow Gun Periodic Resource\";rt=\"Text\";obs", get_gun_status, post_handler, NULL, NULL, PERIOD, get_gun_status_per);
*/

/*---------------------------------------------------------------------------*/

void
get_level(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
	int length = 71, i;
	char s[length];
	char in[7];
	char out[13];
	char temp[11];
	int temp_l = 12;

	snprintf(in, 8, "{\"e\": [");
	strcat(s, in);
	for (i= 0; i < SNOW_DIM; i++)
	{
		snprintf(temp, temp_l, "{\"v\":\"%d\"}", snow_level[i]);
		strcat(s, temp);
		if (i != SNOW_DIM-1)
			strcat(s, ", ");
	}
	snprintf(out, 14, "], \"bn\": \"s\"}");
	strcat(s, out);
	//char * message = s;
	//length = strlen(message);
	length = strlen(s);
	memcpy(buffer, s, length);
	REST.set_header_content_type(response, REST.type.TEXT_PLAIN); 
	REST.set_header_etag(response, (uint8_t *) &length, 1);
	REST.set_response_payload(response, buffer, length);
	printf("CoAP packet sent: '%s', length: %d\n", s, length);
}

/*---------------------------------------------------------------------------*/

void
get_prob(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
	int length = 84, i;
	char s[length];
	char in[7];
	char out[13];
	char temp[11];
	int temp_l = 12;

	snprintf(in, 8, "{\"e\": [");
	strcat(s, in);
	for (i= 0; i < AVAL_DIM; i++)
	{
		snprintf(temp, temp_l, "{\"v\":\"%d\"}", aval_prob[i]);
		strcat(s, temp);
		if (i != AVAL_DIM-1)
			strcat(s, ", ");
	}
	snprintf(out, 14, "], \"bn\": \"a\"}");
	strcat(s, out);
	//char * message = s;
	//length = strlen(message);
	length = strlen(s);
	memcpy(buffer, s, length);
	REST.set_header_content_type(response, REST.type.TEXT_PLAIN); 
	REST.set_header_etag(response, (uint8_t *) &length, 1);
	REST.set_response_payload(response, buffer, length);
	printf("CoAP packet sent: '%s', length: %d\n", s, length);
}

/*---------------------------------------------------------------------------*/

void get_gun_status(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{

	int length = 71, i;
	char s[length];
	char in[7];
	char out[13];
	char temp[11];
	int temp_l = 12;

	snprintf(in, 14, "{\"e\": [");
	strcat(s, in);
	for (i= 0; i < SNOW_DIM; i++)
	{
		char temp[11];
		snprintf(temp, temp_l, "{\"v\":\"%d\"}", snow_gun_array[i]);
		strcat(s, temp);
		if (i != SNOW_DIM-1)
			strcat(s, ", ");
	}
	
	snprintf(out, 14, "], \"bn\": \"g\"}");
	strcat(s, out);
	//char * message = s;
	//length = strlen(message);
	length = strlen(s);
	memcpy(buffer, s, length);
	REST.set_header_content_type(response, REST.type.TEXT_PLAIN); 
	REST.set_header_etag(response, (uint8_t *) &length, 1);
	REST.set_response_payload(response, buffer, length);
	printf("CoAP packet sent: '%s', length: %d\n", s, length);
}

/*---------------------------------------------------------------------------*/

static void get_level_per()
{
	REST.notify_subscribers(&snow_level_mote);
}

/*---------------------------------------------------------------------------*/

static void get_prob_per()
{
	REST.notify_subscribers(&avalanche_mote);
}

/*---------------------------------------------------------------------------*/

static void get_gun_status_per()
{
	REST.notify_subscribers(&snow_gun);
}

/*---------------------------------------------------------------------------*/

static void get_level_trigger()
{
    REST.notify_subscribers(&snow_level_mote);  
}

/*---------------------------------------------------------------------------*/

static void get_prob_trigger()
{
    REST.notify_subscribers(&avalanche_mote);  
}

/*---------------------------------------------------------------------------*/

static void get_gun_status_trigger()
{
    REST.notify_subscribers(&snow_gun);  
}

/*---------------------------------------------------------------------------*/

void
post_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
	int len;
	const char *val = NULL;
	uip_ipaddr_t addr;
	const char limit[2] = "\"";
	char *token, *end;
	int packet[2], temp, i = 0;
	char msg[1];
 
	len = REST.get_request_payload(request, &val);
	printf("LEN: %d, VALUE: %s\n", len, val);
     
	if (len > 0)
	{
		token = strtok(val, limit);

		printf("CoAP packet received: ");
		while(token != NULL)
		{
			temp = strtol(token, &end, 10);
			if (temp != 0)
			{
				packet[i] = temp;	// ([type node_id])	type = 1 => maintenance; type = 2 => snow_gun
									// if (type == 1) => maintenance = 1. If (type == 2) => snow_gun = !snow_gun.
				printf("%d ", packet[i]);
				i++;
			}
			token = strtok(NULL, limit);
		}
		printf("\n");
		
		// invio dati a node_id:
		uip_ip6addr(&addr,0xaaaa,0,0,0,0xc30c,0,0,packet[1]);
		sprintf(msg, "%d", packet[0]);
		simple_udp_sendto(&unicast_connection, msg, strlen(msg) + 1, &addr);
		printf("Sent packet to %d, type %d\n", packet[1], packet[0]);
		REST.set_response_status(response, REST.status.CREATED);
	}
	else
		REST.set_response_status(response, REST.status.BAD_REQUEST);
}

/*---------------------------------------------------------------------------*/
PROCESS(server, "Proxy");
AUTOSTART_PROCESSES(&server);
/*---------------------------------------------------------------------------*/

static void
receiver(struct simple_udp_connection *c,
         const uip_ipaddr_t *sender_addr,
         uint16_t sender_port,
         const uip_ipaddr_t *receiver_addr,
         uint16_t receiver_port,
         const uint8_t *data,
         uint16_t datalen)
{
	const char limit[2] = "\"";
	char *token, *end;
	int packet[3], temp, i = 0;

	token = strtok(data, limit);

	printf("Packet received: ");
	while(token != NULL)
	{
		temp = strtol(token, &end, 10);
		if (temp != 0)
		{
			packet[i] = temp;	// ([type value node_id])
			if (packet[0] == 3 && i == 1)
				packet[i]--;
			printf("%d ", packet[i]);
			i++;
		}
		token = strtok(NULL, limit);
	}
	printf("\n");

	// Update resources:
	
	if (packet[0] == 1)	// node_id partono da un offset, li voglio mettere negli array da posizione 0
	{
		snow_level[(packet[2] - SNOW_OFFSET)] = packet[1];
		snow_level_mote.trigger();
	}
	else if (packet[0] == 2)
	{
		aval_prob[(packet[2] - AVAL_OFFSET)] = packet[1];
		avalanche_mote.trigger();
	}
	else if (packet[0] == 3)
	{
		snow_gun_array[(packet[2] - SNOW_OFFSET)] = (packet[1]);
		snow_gun.trigger();
	}
		
	//printf("Snow: %d\n Aval: %d\n", snow_level[(packet[1] - SNOW_OFFSET)], aval_prob[(packet[1] - SNOW_OFFSET - SNOW_DIM)]);
}

/*----------------------------------------------------------------*/

static void
print_local_addresses(void)
{
  int i;
  uint8_t state;

  PRINTF("Server IPv6 addresses: ");
  for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
    state = uip_ds6_if.addr_list[i].state;
    if(state == ADDR_TENTATIVE || state == ADDR_PREFERRED) {
      PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
      PRINTF("\n");
    
      }
    }
  
}

/*----------------------------------------------------------------*/

PROCESS_THREAD(server, ev, data)
{

	PROCESS_BEGIN();

	  
	simple_udp_register(&unicast_connection, UDP_PORT,
		              NULL, UDP_PORT, receiver);

	rest_init_engine();

	rest_activate_resource(&snow_level_mote, "snow-level");
	rest_activate_resource(&avalanche_mote, "aval-prob");
	rest_activate_resource(&snow_gun, "snow-guns");

	while(1)
	{
		PROCESS_WAIT_EVENT();
	}
	
	PROCESS_END();
}
/*---------------------------------------------------------------------------*/
