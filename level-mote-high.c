#include "contiki.h"
#include "lib/random.h"
#include "sys/etimer.h"
#include "net/ip/uip.h"
#include "net/ip/uip-debug.h"
#include "sys/node-id.h"
#include "net/rpl/rpl.h"

#include "simple-udp.h"

#include <stdio.h>
#include <string.h>

#define UDP_PORT 1234

// PARAMETRI DA CAMBIARE!!!
#define MAX_SNOW_THRESH		400
#define	MIN_SNOW_THRESH		20
#define	change_tolerance	10
#define	DISTR_DIM			202

#define	OPTIMAL_INDEX_RISE	46
#define	OPTIMAL_INDEX_FALL	156
#define	HIGH_INDEX			77
#define	LOW_INDEX			170
#define MAX_INDEX			90
#define MIN_INDEX			185
#define	STEP_INDEX			10

#define	LEVEL_ID			1
#define GUN_ID				3

#define	MAINT_TIME		(CLOCK_SECOND * 30)
#define	CHANGE_TIME		(CLOCK_SECOND * 40)
#define	GUN_TIME		(CLOCK_SECOND * 20)
#define INIT_TIME		(CLOCK_SECOND * 30)

static int distribution[DISTR_DIM] = {2, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 6, 7, 7, 8, 9, 9, 10, 11, 12, 14, 15, 16, 18, 19, 21, 23, 24, 27, 29, 31, 33, 36, 39, 42, 45, 48, 52, 55, 59, 63, 67, 72, 76, 81, 86, 92, 97, 103, 109, 115, 121, 128, 135, 142, 149, 156, 164, 171, 179, 187, 196, 204, 212, 221, 229, 238, 247, 256, 265, 273, 282, 291, 300, 308, 317, 325, 333, 341, 349, 357, 364, 371, 378, 385, 391, 397, 403, 408, 413, 417, 421, 424, 428, 430, 432, 434, 435, 436, 436, 436, 435, 434, 432, 430, 428, 424, 421, 417, 413, 408, 403, 397, 391, 385, 378, 371, 364, 357, 349, 341, 333, 325, 317, 308, 300, 291, 282, 273, 265, 256, 247, 238, 229, 221, 212, 204, 196, 187, 179, 171, 164, 156, 149, 142, 135, 128, 121, 115, 109, 103, 97, 92, 86, 81, 76, 72, 67, 63, 59, 55, 52, 48, 45, 42, 39, 36, 33, 31, 29, 27, 24, 23, 21, 19, 18, 16, 15, 14, 12, 11, 10, 9, 9, 8, 7, 7, 6, 5, 5, 4, 4, 4, 3, 3, 3, 2, 2, 2, 2};

static struct simple_udp_connection unicast_connection;
static int maintenance = 0;
static int snow_gun = 0;
static int disable_gun = 0;

/*----------------------------------------------------------------*/

static void send_update (int value, int type)
{
	uip_ipaddr_t addr;
	char msg[42];
	sprintf(msg, "{\"e\": [{\"n\":\"%d\", \"v\":\"%d\"}] \"bn\": \"%d\"}", type, value, node_id);
	uip_ip6addr(&addr,0xaaaa,0,0,0,0xc30c,0,0,0x0002);
	simple_udp_sendto(&unicast_connection, msg, strlen(msg) + 1, &addr);
  	//uip_debug_ipaddr_print(&addr);
	//printf("\n");
	//printf("Invio aggiornamento: '%s'\n", msg);
	//printf("Destinatario: ");
	//uip_debug_ipaddr_print(&addr);
	//printf("\n");
}

/*----------------------------------------------------------------*/

static void receiver(struct simple_udp_connection *c,
			 const uip_ipaddr_t *sender_addr,
			 uint16_t sender_port,
			 const uip_ipaddr_t *receiver_addr,
			 uint16_t receiver_port,
			 const uint8_t *data,
			 uint16_t datalen)
{
	printf("Packet received from ");
	uip_debug_ipaddr_print(sender_addr);
	if (datalen > 0 && atoi(data) == 1)
	{
		maintenance = 1;
		printf(": maintenance = %d\n", maintenance);
	}
	else if (datalen > 0 && atoi(data) == 2)
	{
		snow_gun = (snow_gun == 1)? 0 : 1;
		disable_gun = (snow_gun == 1)? 0 : 1;		
		send_update(snow_gun+1, GUN_ID);
		printf(": snow gun = %d\n", snow_gun);
	}
}

/*----------------------------------------------------------------*/

static void
print_local_addresses(void)
{
  int i;
  uint8_t state;

  PRINTF("Server IPv6 addresses: ");
  for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
    state = uip_ds6_if.addr_list[i].state;
    if(state == ADDR_TENTATIVE || state == ADDR_PREFERRED) {
      PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
      PRINTF("\n");
    
      }
    }
  
}

/*----------------------------------------------------------------*/

PROCESS(level_mote, "Snow level mote");
AUTOSTART_PROCESSES(&level_mote);

/*----------------------------------------------------------------*/

PROCESS_THREAD(level_mote, ev, data)
{
	// initialization:
	static struct etimer	level_timer, maint_timer, gun_timer;
	static struct etimer	init_timer;
	static int index = HIGH_INDEX;
	static int snow_level;
	
	snow_level = distribution[index];

	PROCESS_BEGIN();
	
	simple_udp_register(&unicast_connection, UDP_PORT, NULL, UDP_PORT, receiver);
	
	etimer_set(&init_timer, INIT_TIME);
	// wait until proxy has initialized:
	PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&init_timer));
	etimer_reset(&init_timer);
	etimer_set(&level_timer, CHANGE_TIME);
	
	while(1)
	{
		printf("Index: %d\n", index);
		if (/*(index < MAX_INDEX || index > MIN_INDEX) || */(snow_level <= MAX_SNOW_THRESH + change_tolerance && snow_level >= MIN_SNOW_THRESH - change_tolerance))
		{	// generate snow level changes only if within a certain tolerance:
			snow_level = distribution[index];
			index = (index + 1) % DISTR_DIM;
			printf("Index after update: %d\n", index);
		}
		// send snow_level packet:
		send_update(snow_level, LEVEL_ID);
		// wait until next sensing
		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&level_timer));
		etimer_reset(&level_timer);

		if (snow_level > MAX_SNOW_THRESH && maintenance == 1)
		{	// maintenance notification received
			etimer_set(&maint_timer, MAINT_TIME);
			printf("MAINTENANCE STARTED: %d\n", maintenance);
			// wait until maintenance has finished:
			PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&maint_timer));
			etimer_reset(&maint_timer);
			// reset to optimal index:
			index = OPTIMAL_INDEX_FALL;
			// reset maintenance variable
			maintenance = 0;
			printf("MAINTENANCE FINISHED: %d\n", maintenance);
			printf("Index after maintenance: %d\n", index);
		}
		if (snow_level < MIN_SNOW_THRESH && !disable_gun)
		{	// cannon must be enabled and proxy notified:
			snow_gun = 1;
			send_update(snow_gun+1, GUN_ID);
			// wait and send snow level to proxy:
			etimer_set(&init_timer, (CLOCK_SECOND * 3));
			PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&init_timer));
			etimer_reset(&init_timer);
			send_update(snow_level, LEVEL_ID);
		}
		
		if (snow_gun == 1)
		{	// cannon is active
			etimer_set(&gun_timer, GUN_TIME);
			// wait until end of operation:
			PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&gun_timer));
			etimer_reset(&gun_timer);
			// reset to optimal index:
			if (snow_level < MIN_SNOW_THRESH)
				index = OPTIMAL_INDEX_RISE;
			else if (index <= (DISTR_DIM/2)) {
				index = (index + 10 >= (DISTR_DIM/2))? (DISTR_DIM/2) : (index + STEP_INDEX);
				printf("Index increased\n");
			}
			else if (index > DISTR_DIM/2) {
				index = (index - 10 <= (DISTR_DIM/2))? (DISTR_DIM/2) : (index - STEP_INDEX);				
				printf("Index decreased\n");
			}
			// disable cannon and send notification:
			snow_gun = 0;
			send_update(snow_gun+1, GUN_ID);
			printf("Index after snowgun: %d\n", index);
		}
		// set timer for next sensing
		etimer_set(&level_timer, CHANGE_TIME);
	}

	PROCESS_END();
}
