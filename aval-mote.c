#include "contiki.h"
#include "lib/random.h"
#include "sys/etimer.h"
#include "net/ip/uip.h"
#include "net/ip/uip-debug.h"
#include "sys/node-id.h"
#include "net/rpl/rpl.h"

#include "simple-udp.h"

#include <stdio.h>
#include <string.h>
#include <math.h>


#define UDP_PORT 1234

#define	CHANGE_TIME		(CLOCK_SECOND * 60)
#define INIT_TIME		(CLOCK_SECOND * 30)

//#define	CHANGE_TIME		(CLOCK_SECOND * 60)

#define	AVAL_ID			2
#define	OPTIMAL_INDEX	0
#define	HIGH_INDEX		83
#define	DISTR_DIM		195


static struct simple_udp_connection unicast_connection;

static int distribution[DISTR_DIM] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 10, 10, 11, 12, 13, 13, 14, 15, 16, 17, 19, 20, 21, 22, 24, 25, 26, 28, 29, 31, 32, 34, 36, 37, 39, 41, 43, 45, 47, 49, 50, 52, 54, 56, 58, 60, 63, 65, 67, 69, 70, 72, 74, 76, 78, 80, 82, 83, 85, 87, 88, 89, 91, 92, 93, 94, 95, 96, 97, 98, 98, 99, 99, 100, 100, 100, 100, 100, 99, 99, 98, 98, 97, 96, 95, 94, 93, 92, 91, 89, 88, 87, 85, 83, 82, 80, 78, 76, 74, 72, 70, 69, 67, 65, 63, 60, 58, 56, 54, 52, 50, 49, 47, 45, 43, 41, 39, 37, 36, 34, 32, 31, 29, 28, 26, 25, 24, 22, 21, 20, 19, 17, 16, 15, 14, 13, 13, 12, 11, 10, 10, 9, 8, 8, 7, 7, 6, 6, 5, 5, 4, 4, 4, 3, 3, 3, 3, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};


/*----------------------------------------------------------------*/

static void receiver(struct simple_udp_connection *c,
			 const uip_ipaddr_t *sender_addr,
			 uint16_t sender_port,
			 const uip_ipaddr_t *receiver_addr,
			 uint16_t receiver_port,
			 const uint8_t *data,
			 uint16_t datalen)
{
	printf("Data received on port %d from port %d with length %d\n",
		receiver_port, sender_port, datalen);
}

/*----------------------------------------------------------------*/

static void send_update (int value, int type)
{
	uip_ipaddr_t addr;
	char msg[42];
	sprintf(msg, "{\"e\": [{\"n\":\"%d\", \"v\":\"%d\"}] \"bn\": \"%d\"}", type, value, node_id);
	uip_ip6addr(&addr,0xaaaa,0,0,0,0xc30c,0,0,0x0002);
	simple_udp_sendto(&unicast_connection, msg, strlen(msg) + 1, &addr);
	printf("Invio aggiornamento: '%s'\n", msg);
	printf("Destinatario: ");
	uip_debug_ipaddr_print(&addr);
	printf("\n");
}

/*----------------------------------------------------------------*/

PROCESS(avalanche_mote, "Avalanche mote");
AUTOSTART_PROCESSES(&avalanche_mote);

/*----------------------------------------------------------------*/

PROCESS_THREAD(avalanche_mote, ev, data)
{
	// initialization:
	static struct etimer	prob_timer,
							init_timer;
	static uint8_t	index = OPTIMAL_INDEX,
					prob;


	PROCESS_BEGIN();
	
	simple_udp_register(&unicast_connection, UDP_PORT, NULL, UDP_PORT, receiver);
	
	etimer_set(&init_timer, INIT_TIME);
	// wait until proxy has initialized:
	PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&init_timer));
	etimer_reset(&init_timer);
	etimer_set(&prob_timer, CHANGE_TIME);

	while(1)
	{
		// generate change:
		if (distribution[index] >= 100)
			prob = 100;
		else
			prob = distribution[index];
		index = (index + 1) % DISTR_DIM;
		// Send value to proxy:
		send_update(prob, AVAL_ID);
		// wait until next sensing:
		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&prob_timer));
		etimer_reset(&prob_timer);
		// set timer for next sensing:
		etimer_set(&prob_timer, CHANGE_TIME);
	}

	PROCESS_END();
}
